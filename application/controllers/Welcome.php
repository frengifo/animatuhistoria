<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
	}

	function index()
	{
		$this->load->database();

		$this->db->where('status', 1 );
		$query = $this->db->get( 'registro' );
		$data['links'] = $query->result();
		$data['cantidad'] = $query->num_rows();
		
		$this->load->view('welcome_message', $data);
	}


	public function register(){
		$this->load->database();
		if($this->input->post('nombres') != "")
		{
			

			$this->db->where('email', $this->input->post('email') );
		    $query = $this->db->get('registro');
		    if ($query->num_rows() > 0){
		        
		    	echo json_encode( array('success' => false, 'message' => 'Parece que usted ya se encuentra participando en el concurso :)' ) );

		    }
		    else{
		        $data = array(
				        'nombres' => $this->input->post('nombres'),
				        'dni' => $this->input->post('dni'),
				        'email' => $this->input->post('email'),
				        'empresa' => $this->input->post('empresa'),
				        'link' => $this->input->post('link'),
				        'mobil' => $this->isMobile() ? 1:0
				);

				$id = $this->db->insert('registro', $data);

				if ($id) {
					echo json_encode( array('success' => true ) );
				}else{
					echo json_encode( array('success' => false, 'message' => 'Ocurrió un error, por favor intentelo denuevo :)' ) );
				}
		    }

			


		}
		
	}

	public function isMobile() {
	    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */