<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');
		$this->load->library('tank_auth');
		$this->load->library('grocery_CRUD');

		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		}
	}

	public function _example_output($output = null)
	{
		$this->load->view('example.php',$output);
	}

	

	public function index()
	{
		try{

			$crud = new grocery_CRUD();

			$crud->set_table('registro');

			$crud->unset_texteditor('link');

			$crud->set_language("spanish");

			$crud->field_type('status','dropdown',
			array('1' => 'Aprobado', '0' => 'Pendiente', '2' => 'Spam' ));

			$output = $crud->render();

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	

	

}