<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Anima tu historia</title>
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('/') ?>assets/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('/') ?>assets/css/main.css?v=<?php echo rand(0,100) ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('/') ?>assets/js/vendor/fullpage/jquery.fullPage.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet">
	<script src="<?php echo site_url('/') ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo site_url('/') ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('/') ?>assets/js/vendor/fancybox/source/jquery.fancybox.css">
    <link rel="shortcut icon" href="<?php echo site_url('/') ?>favicon.ico" type="image/x-icon" >
    <meta property="og:locale" content="es_ES" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Anima tu Historia" />
	<meta property="og:description" content="Un video animado interactúa mejor con tus clientes por eso participa del concurso “Anima tu historia” y podrás ganar un video animado para tu empresa." />
	<meta property="og:url" content="http://animatuhistoria.com/" />
	<meta property="og:site_name" content="Anima tu Historia" />
	<meta property="og:image" content="<?php echo site_url(); ?>assets/img/share-fb.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <style type="text/css">

    	
    	
    </style>
    <div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.8&appId=1793373284281179";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-86191667-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<style type="text/css">
		.fancybox-nav{
			width: 10%;
		}
	</style>
</head>
<body>	
	<header>
		<div class="container">
			<a href="<?php echo site_url('/') ?>" class="logo">
				<img src="<?php echo site_url('/') ?>assets/img/logo.png" alt="Logo RederDesign" class="one active">
			</a>
			<nav>
				<ul id="menu">
					<li data-menuanchor="inicio"> <a  href="#inicio">Inicio</a> </li>
					<li data-menuanchor="mecanica"> <a href="#mecanica" onclick="ga('send', 'event','Web-Contenido','ver-mecanica-clic','ver-mecanica-menu')">Mecánica</a> </li>
					<li data-menuanchor="registro"> <a href="#registro" onclick="ga('send', 'event','Web-Conversión','ir-participar-clic','participar-menu')">Participa</a> </li>
					<li data-menuanchor="premios"> <a href="#premios" onclick="ga('send', 'event','Web-Contenido','ver-premios-clic','ver-premios-menu')">Premios</a> </li>
					<li data-menuanchor="historias"> <a href="#historias" onclick="ga('send', 'event','Web-Contenido','ver-historias-clic','ver-historia-menu')">Historias</a> </li>
					<!--<li data-menuanchor="ganador"> <a href="#ganador" onclick="ga('send', 'event','Web-Contenido','ver-ganador-clic','ver-ganador-menu')">Ganador</a> </li>-->
				</ul>
				<div id="nav-icon3">
				  <span></span>
				  <span></span>
				  <span></span>
				  <span></span>
				</div>
			</nav>

		</div>
	</header>
	<aside>
		<a href="https://www.facebook.com/AnimaTuHistoria/?fref=ts" target="_blank" onclick="ga('send', 'event','Web-Conversión','Ir-facebook-campania-clic','Ir-facebook-campania')"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		<a href="https://www.youtube.com/watch?v=xWgkFWZKQSA" target="_blank" onclick="ga('send', 'event','Web-Conversión','Ir-youtube-campania-clic','Ir-youtube-campania')"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
	</aside>
	<div id="fullpage">
		<div class="section home" id="section1">
			<div class="container">
				<h1>¡Cuéntanos tu historia de emprendimiento <br> e inspira a otros a través de un video!</h1>
				
				<div data-easy-embed="youtube:xWgkFWZKQSA" class="easy-embed" id="yt-home">
					<div class="overlay-play"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></div>
				</div>
				<h3>
					¡Gana un video animado y asesoramiento en Marketing Digital para tu marca!
				</h3>
				<span data-menuanchor="registro"><a href="#registro" class="btn-white" onclick="ga('send', 'event','Web-Conversión','participa-home-clic','participar-home')
;">¡Participa ya!</a> </span>
			</div>
		</div>
		<div class="section mecanica" id="section2">
			<div class="container">
				<div class="slick">
					<article>
						<img src="<?php echo site_url('/') ?>assets/img/mobil.png" alt="Graba un Video">
						<h2>Graba un Video</h2>
						<p>
							Grábate con tu celular contándonos tu historia de emprendimiento
						</p>

						<h4>Duración máxima de 1:30 min.</h4>
					</article>

					<article>
						<img src="<?php echo site_url('/') ?>assets/img/youtube-icono.png" alt="Sube el video a Youtube">
						<h2>Sube el video <br>a Youtube</h2>
						<p>
							Y ponle como título “Mi historia / (el nombre de tu emprendimiento)”
						</p>

					</article>

					<article>
						<img src="<?php echo site_url('/') ?>assets/img/registro.png" alt="Registrate">
						<h2>Regístrate</h2>
						<p>
							Llena tus datos, regístrate y pega el Link.<br>
							<strong>¡Te tomará menos de 1 minuto!</strong>
						</p>

					</article>
				</div>
				<div class="clear"></div>
				<h3>Recuerda que cada historia debe ser única e irrepetible.</h3>
				<span data-menuanchor="registro"><a href="#registro" class="btn-white">¡Participa ya!</a> </span>
			</div>
		</div>
		<div class="section white register" id="section3">
			<div class="container">
				
				

				<form class="box-form" action="#" method="post" data-parsley-validate="" id="form">
					<h2>Regístrate</h2>
					<label>Mis datos</label>
					<input type="text" name="nombres" placeholder="Nombre y Apellido" data-parsley-pattern="^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$" required>
					<input type="text" name="dni" placeholder="DNI" class="col-2 one" data-parsley-type="digits" required>
					<input type="email" name="email" placeholder="Correo electrónico" class="col-2 two" required>

					<label>Mi empresa</label>
					<input type="text" name="empresa" placeholder="Nombre de mi empresa" required>
					<input type="url" name="link" placeholder="Link de mi video" required>
					<span id="msg"></span>
					<!--<p>¿Sabes cómo subir un video a YouTube? <strong><a href="javascript:;">Aquí de damos los pasos.</a></strong></p>-->

					<label for="terms" id="chk"  >
						<input type="checkbox" name="terms" id="terms" required /><a class="various" href="#terminos" onclick="ga('send', 'event','Web-Contenido','ver-tyc-clic','ver-tyc')">Acepto los Términos y Condiciones establecidos por Reder Design</a>
					</label>

					<button type="submit" class="btn-send btn-black">Enviar</button>

				</form>

				<div class="after hidden">
					<h2>
						Tu historia fue registrada
					</h2> 

					<p>Resultados: Jueves 10 de Noviembre</p>

					<a href="javascript:;" onclick="share('http://animatuhistoria.com/');" class="btn-black">Compartir con amigos</a>
					<span data-menuanchor="historias"><a href="#historias" class="btn-black">Ver otras historias</a> </span>

				</div>
				<div class="hidden">
					<div id="terminos">
						<h3>Términos y condiciones del concurso “Anima tu historia”</h3>
						<h4><strong>DURACIÓN DEL CONCURSO:</strong></h4>
						<p>El presente concurso entrará en vigencia del  24 de Octubre del 2016

							a las 11:00 horas de la mañana hasta el 7 de Noviembre del 2016

							hasta las 18:00 horas de la tarde.</p>
						<p><strong>1. DETALLES DEL DE LA SELECCIÓN EDL VIDEO:</strong></p>
						<p>1.1 Fecha de la selección: miércoles 9 de noviembre del 2016.</p>
						<p>1.2 Horario de la selección: a las 11:00 horas de la mañana.</p>
						<p>1.3 Lugar del sorteo: La selección del video ganador se llevará a cabo

							en nuestra oficina, ubicada en  Jr. Andreas Vesalio 213, Distrito de

							San Borja, Provincia y Departamento de Lima.</p>

						<p><strong>2. ALCANCES DEL CONCURSO</strong></p>
						
						<p>2.1 Participantes del concurso:</p>

						<p>
							- Podrán participar en este concurso personas mayores de 18 años

								que cuenten con una marca.
						</p>
						<p>
							*Solo es válida una historia por marca.
						</p>
						<p>
							- El ganador del premio tiene un periodo de 15 días para iniciar el

							proyecto con nosotros, de no realizarse dentro de la fecha, el premio

							del concurso será para el segundo lugar.
						</p>
						<p>
							- No es necesario comprar un servicio o hacer un pago de algún tipo

							para participar o ganar este concurso.
						</p>

						<p>2.2 Objetivo y Mecánica del concurso:</strong></p>

						<p>
							El mecanismo de este concurso consiste en:<br>

							(i) Ingresa a la web <a href="/">www.animatuhistoria.com.</a><br>

							(ii) Registrar sus datos (nombres, apellidos completos, correo electrónico, teléfonos de contacto, marca, rubro)<br>

							(iii) Grabar un video contándonos tu historia de emprendimiento, con el

							objetivo de  inspirar a otros emprendedores a hacer realidad sus

							proyectos. (Duración máxima de 1:30 min) y comparte el link. En caso

							de ganar,  le realizaremos un video animado y 1 consultoría de

							marketing digital gratis.<br>

							(iv) Asimismo, cada historia tiene que ser única e irrepetible.
						</p>

						<p><strong>3. DETALLE DEL CONCURSO, PREMIO Y ENTREGA:</strong></p>


						<p>3.1. Detalles del sorteo:</p>

						<p>- Selección de ganadores: miércoles 9 de noviembre a las 11:00 am</p>

						<p>- Anuncio de ganadores: jueves 10 de noviembre a las 11:00 am </p>

						<p>- Cantidad de ganadores: 2</p>

						<p>- Premios: Un video animado y una consultoría de marketing digital.</p>

						<p>3.2. Especificaciones del video:</p>

						<p>- La persona contará su historia de emprendimiento con el objetivo de motivar a otras personas a emprender un negocio.</p>

						<p>3.3 Criterios para seleccionar al ganador</p>

						<p>- Creatividad al contar la historia.</p>

						<p>- Producto innovador.</p>

						<p>3.4. Detalle sobre el premio:</p>

						<p>A la historia que más inspire se le realizará un video animado que

						contará con una duración máxima de 1:30 minutos y al segundo

						ganador la consultoría de marketing digital para apoyar su

						emprendimiento.</p>

						<p>3.5. Especificaciones sobre la entrega de los premios:</p>

						<p>- Se publicará el nombre de los ganadores el 10 de noviembre. Los

							ganadores serán contactados telefónicamente y/o por correo

							electrónico, para así agendar una reunión que ayude a definir los

							detalles de la creación del video animado. En caso de no lograr

							comunicarnos con el ganador del concurso, este podrá ingresa a

							www.animatuhistoria.com y verificar si ganó o no el premio. Si es el

							caso, debe reclamar su premio dentro de un plazo máximo de quince

							(15) días calendario.</p>

						<p>- Para registrarse en el concurso, todos los participantes deben  llenar

							sus datos personales y contar su historia sobre el origen de su marca

							de forma creativa.</p>

						<p><strong>4. ASPECTOS LEGALES ADICIONALES:</strong></p>

						<p>- Solo podrán participar peruanos mayores de dieciocho (18) que

						cuenten con una marca. Cada participante asume la responsabilidad

						por la veracidad de la información registrada en la web

						www.animatuhistoria.com, los participantes se comprometen en hacer

						uso adecuado y lícito de la plataforma en la campaña “Anima tu

						historia” de conformidad con la legislación aplicable y  las presentes

						bases.</p>

						<p>- Los premios no serán transferibles, ni canjeables por ningún motivo.

							Y será otorgado únicamente por quien sea notificado como ganador.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="section premio" id="section4">
			<div class="container">
				<h2>Premios</h2>
				<article>
					<div class="box">
						<h3>Video Animado <br> <small>para tu negocio</small></h3>
						<a href="https://www.youtube.com/embed/MyIawjuXQA0?autoplay=1" class="various fancybox.iframe" onclick="ga('send', 'event','Web-Contenido','ver-caso-clic','ver-caso')">VER CASOS DE CLIENTES</a>
					</div>
					<div class="box">
						<img src="<?php echo site_url('/') ?>assets/img/la-mejor-historia.png" alt="La mejor hitoria">
					</div>
					<div class="box">
						<h3><small>Asesoramiento en</small><br> Marketing Digital</h3>
					</div>
				</article>
				 <span data-menuanchor="historias"><a href="#historias" class="btn-white" onclick="ga('send', 'event','Web-Conversión','ver-historias-premio-clic','ver-historias-premio')">Ver historias</a> </span>
				 <span data-menuanchor="registro"><a href="#registro" class="btn-white" onclick="ga('send', 'event','Web-Conversión','ir-participar-premio-clic','ir-participar-premio')">¡Participa ya!</a> </span>

			</div>
		</div>

		<div class="section historia" id="section5">
			<div class="container">
				<h2>Historias</h2>
				<div id="content"></div>
    			<ul id="pagination"></ul>
    			<span data-menuanchor="registro"><a href="#registro" class="btn-black">Sube tu historia aquí</a>
			</div>



			<div class="hidden">

				<?php if ( isset($_GET["v"]) ): ?>
					
					<?php 
					$url_share = "http://animatuhistoria.com/?v=".$_GET["v"]."#historias";
					$anchor = "<a href='javascript:;' onclick='share(&quot;".($url_share)."&quot;);'><small>Compartir en</small><br> Facebook</a>";  
					?>
							<a href="http://www.youtube.com/embed/<?php echo $_GET["v"]; ?>?autoplay=1" id="video_auto" class="box various fancybox.iframe" title="<?php echo $anchor; ?>" onclick="ga('send', 'event','Web-Conversión','ver-historia-int-clic','ver-historia-int')" >
								
							<div data-easy-embed="youtube:<?php echo $_GET["v"]; ?>" class="easy-embed" id="yt-auto">
									<div class="overlay-play"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></div>
								</div>

							</a>

				<?php endif ?>


				<?php $i=0; $j = 1; $o=0; ?>
				<?php 
					$items_per_page = 9;
					$pages = ceil($cantidad / $items_per_page);

					
					var_dump($pages);
				?>

				<?php foreach ($links as $key => $value): ?>
					<?php $o++; ?>
					<?php if ($i == 0): ?>
						
						<div class="page page-<?php echo $j; ?>">

					<?php endif ?>


							<?php 

								parse_str( parse_url( $value->link, PHP_URL_QUERY ), $query_vars );
								$url_share = "http://animatuhistoria.com/?v=".$query_vars['v']."#historias";
							?>

							<?php $anchor = "<a href='javascript:;' onclick='share(&quot;".($url_share)."&quot;);'><small>Compartir en</small><br> Facebook</a>";  ?>
							<a href="http://www.youtube.com/embed/<?php echo $query_vars['v']; ?>?autoplay=1" rel="gal-1" onclick="setBgImage(this);" class="box various fancybox.iframe" title="<?php echo $anchor; ?>" >
								
								<div data-easy-embed="youtube:<?php echo $query_vars['v']; ?>" class="easy-embed" id="yt-<?php echo $o; ?>">
									<div class="overlay-play"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></div>
								</div>

							</a>

					<?php $i++; ?>

					<?php if ($i == $items_per_page): ?>

						<?php 

							$i=0; 
							$j++; 



						?>


						</div>
					<?php endif ?>
					
					<?php if ($o == count($links) && $i < $items_per_page): ?>

						</div>
						
					<?php endif ?>


				<?php endforeach ?>
			</div>
			<style type="text/css">
				
			</style>
			<script type="text/javascript"> var pages = <?php echo $pages < 1 ? 1:$pages; ?></script>
		</div>

		<!--
		<div class="section winner" id="section6">

			<div class="container">
				
				<h2>Felicitaciones a Robertho</h2>

				<p>¡Echa un vistazo a la historia ganadora!</p>

				
				<div data-easy-embed="youtube:YXC61XMnDRM" class="easy-embed" id="yt-winner">
					<div class="overlay-play"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></div>
				</div>
			</div> 

			<footer class="text-center">
				<img src="<?php echo site_url('/') ?>assets/img/logo-footer.png" class="logo-footer">
				<h3>¡Contáctanos!</h3>
				<h4>
					<span class="cel"><i class="fa fa-whatsapp" aria-hidden="true"></i> 989258303</span> <span class="tel"><i class="fa fa-phone" aria-hidden="true"></i> 01 4936303</span> <span class="email"><i class="fa fa-envelope" aria-hidden="true"></i> hola@reder.pe</span>  
				</h4>

				<h5>Jr. Andreas Vesalio 213, San Borja</h5>

				<p>Reder Design <i class="fa fa-copyright" aria-hidden="true"></i> 2016</p>

			</footer>

		</div>
		-->

	</div>

</body>
<script type="text/javascript" src="<?php echo site_url('/') ?>assets/js/vendor/fullpage/jquery.fullPage.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('/') ?>assets/js/vendor/fancybox/source/jquery.fancybox.pack.js"></script>

<script type="text/javascript" src="<?php echo site_url('/') ?>assets/js/plugins.js?v=<?php echo rand(0,100); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('/') ?>assets/js/main.js?v=<?php echo rand(0,100); ?>"></script>
<script type="text/javascript">
	var site_url ="<?php echo site_url('/') ?>";
</script>
</html>