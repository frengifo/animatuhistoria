
jQuery(document).ready(function($) {
      

    /*REG*/
    jQuery('#form').submit(function(e) { 
        e.preventDefault();
        jQuery("#msg").html("");
        if ( jQuery(this).parsley().isValid() ) {
          jQuery("#form").addClass("loading");
          jQuery.post( '/welcome/register', {

              action: 'create_custom_user',
              nombres: jQuery(this).find("input[name='nombres']").val(),
              dni: jQuery(this).find("input[name='dni']").val(),
              email: jQuery(this).find("input[name='email']").val(),
              empresa: jQuery(this).find("input[name='empresa']").val(),
              link: jQuery(this).find("input[name='link']").val(),
              
          }, function(data){
            jQuery("#form").removeClass("loading");
            if (data.success) {
              jQuery("#form").addClass("hidden");
              jQuery(".after").removeClass("hidden");
              ga('send', 'event','Web-Conversión','enviar-registro-clic','enviar-registro');
            }else{
              jQuery("#msg").html(data.message);
            }
          }, 'json');

        }
    });

    $('#nav-icon3').click(function(){
      $(this).toggleClass('open');
      $("#menu").slideToggle();
    });

    if ( $(window).width() < 768 ) {
      $('.slick').slick({
        dots: true,
        arrows: false
      }); 
    }

    $(window).click(function() {
      
      if ( $(window).width() < 768 ) {

        $("#menu").slideUp();
        $("#nav-icon3").removeClass('open'); 

        

      }

      

    });

    $("#nav-icon3").click(function(event){
        event.stopPropagation();
    });

    $('#fullpage').fullpage({
    	anchors: ['inicio', 'mecanica', 'registro', 'premios', 'historias', 'ganador'],
		  menu: '#menu',
      autoScrolling:false,

		  afterLoad: function(anchorLink, index){
            var loadedSection = $(this);

            //using anchorLink
            if(anchorLink == 'registro' || anchorLink == 'historias'){
                console.log("Registro!");
                $("header nav").addClass("color-black");
                $("aside a").addClass("blue");
            }else{
                $("aside a").removeClass("blue");
                $("header nav").removeClass("color-black");

            }
        }
    });

    $.fn.fullpage.setFitToSection(false);

    $('#pagination').twbsPagination({
        totalPages: pages,
        visiblePages: 5,
        prev: '&#10094;',
        next: '&#10095;',
        onPageClick: function (event, page) {
        	
          var html = $(".page-"+page).html();
          $('#content').html(html);

          $("#content a").each(function(index, el) {
      
            $(this).find(".easy-embed").easyEmbed();

          });

        }
    });

    $(".various").fancybox({
		maxWidth	: 1024,
		maxHeight	: 600,
		fitToView	: false,
		width		: '90%',
		height		: '90%',
		autoSize	: false,
		closeClick  : false, // prevents closing when clicking INSIDE fancybox
		helpers     : { 
			overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
		},
		openEffect	: 'none',
		closeEffect	: 'none',
		padding : 0,
		afterShow: function(){
			
      

		}
	});

    $('#yt-winner').easyEmbed();
    $('#yt-home').easyEmbed();

    

    
    
    
});


setTimeout( function(){
  
  if ( jQuery("#video_auto").length > 0 ) {
    jQuery('#yt-auto').easyEmbed();

    
    bgImage = jQuery('#video_auto').find(".easy-embed").css('background-image').replace(/^url|[\(\)]/g, '');
    bgImage = bgImage.replace(/^"(.*)"$/, '$1');

    jQuery('#video_auto').trigger('click');

  }

}, 3000);

var bgImage ="";
function share(url){

  console.log( encodeURIComponent(url) );
  console.log(url);
  ga('send', 'event','Web-Conversión','compartir-historia-clic','compartir-historia');

  if ( url == "http://animatuhistoria.com/") {
  	FB.ui({
	    method: 'share',
	    display: 'popup',
      title: 'Participa en Anima tu Historia',  // The same than name in feed method
      picture: "http://animatuhistoria.com/assets/img/share-fb.png",  
      caption: 'Un video animado interactúa mejor con tus clientes por eso participa del concurso “Anima tu historia” y podrás ganar un video animado para tu empresa',
	    href: (url),
	  }, function(response){});
  }else{
  	FB.ui({
	    method: 'share',
	    display: 'popup',
      title: 'Participa en Anima tu Historia',  // The same than name in feed method
      picture: bgImage,  
      caption: 'Un video animado interactúa mejor con tus clientes por eso participa del concurso “Anima tu historia” y podrás ganar un video animado para tu empresa',
	    href: (url),
	  }, function(response){});
  }

	

}


function setBgImage(ele){

  bgImage = $(ele).find(".easy-embed").css('background-image').replace(/^url|[\(\)]/g, '');
  bgImage = bgImage.replace(/^"(.*)"$/, '$1');

}